# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Grit::Application.config.secret_key_base = 'a68b2989a65dbb62cf72dc71732950c57ab7472134f8cd728751406413caecaa4fe6913ec64d4810fafc78bed4b8f1aa9d9b4a78e28010a6fe5650dcc28e5ba9'
