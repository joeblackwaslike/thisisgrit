# Tells Rails to load our ApplicationSettings module and then populates our 
# config class variable with data from our any parsed yaml file(s)
#
# From here on in, we should be able to call:
#
#   ApplicationSettings.config['email_notifications']
#
# and have it return our config option

if(ActiveRecord::Base.connection.table_exists? 'user_settings' and UserSetting.all.count > 0)

  require "#{Dir.pwd}/lib/application_settings.rb"

  t_settings = UserSetting.find_by_key('template').user_settings
  template = {}
  for ts in t_settings
    template[ts.key] = ts.value
  end

  yt_settings = UserSetting.find_by_key('youtube').user_settings
  youtube = {}
  for yts in yt_settings
    youtube[yts.key] = yts.value
  end

  fv_settings = UserSetting.find_by_key('featured_videos').user_settings
  featured_videos = []
  for fvs in fv_settings
    thishash = Hash.new
    thishash['key'] = fvs.value
    featured_videos << thishash
  end

  ApplicationSettings.config = {'template' => template, 'youtube' => youtube, 'featured_videos' => featured_videos}

end

#ApplicationSettings.config = YAML.load_stream(UserSetting.find_by_key('settings-yml').value.to_yaml)