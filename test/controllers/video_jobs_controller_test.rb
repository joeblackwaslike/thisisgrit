require 'test_helper'

class VideoJobsControllerTest < ActionController::TestCase
  setup do
    @video_job = video_jobs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:video_jobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create video_job" do
    assert_difference('VideoJob.count') do
      post :create, video_job: {  }
    end

    assert_redirected_to video_job_path(assigns(:video_job))
  end

  test "should show video_job" do
    get :show, id: @video_job
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @video_job
    assert_response :success
  end

  test "should update video_job" do
    patch :update, id: @video_job, video_job: {  }
    assert_redirected_to video_job_path(assigns(:video_job))
  end

  test "should destroy video_job" do
    assert_difference('VideoJob.count', -1) do
      delete :destroy, id: @video_job
    end

    assert_redirected_to video_jobs_path
  end
end
