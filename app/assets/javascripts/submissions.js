$(document).ready(function(){

  // Create userDetails object, grab camera, attach userDetails to camera,
  // hide step1, show step2, set full width
  $('#video-submissions #steps_form .step1 .next').click(function(e) {
    e.preventDefault();
    var userDetails = ({
        name: $('#steps_form .step1 input[type="text"]').val(),
        email: $('#steps_form .step1 input[type="email"]').val()
    });
    var thisCamera = CameraTag.cameras["grit"];
    thisCamera.addVideoData(userDetails);
    hideStepOne();
    showStepTwo();
    setFullWidth();
  });

  var stepOne = ['#steps_form .step1', '#steps_count .step1'];
  var stepTwo = ['#steps_form .step2', '#steps_count .step2'];
  var wizard = ['#wizard', '#steps_count'];
  var showElementWithEffect = function(element) {
    $(element).show("slide", {direction: "left"})
  }
  var hideElementWithEffect = function(element) {
    $(element).hide("slide", {direction: "left"})
  }
  var attachErrorHandler = function(handler) {
    $('#video-submissions .error a.try-again').click(handler);
  }

  // Show, Hide Helpers
  var hideStepOne = function() {
    $.each(stepOne, function(i, element) {
      $(element).hide("slide", {direction: "left"});
    });
  };
  var showStepOne = function() {
    $.each(stepOne, function(i, element) {
      showElementWithEffect(element);
    });
  };
  var hideStepTwo = function() {
    $.each(stepTwo, function(i, element) {
      $(element).hide("slide", {direction: "left"});
    });
  };
  var showStepTwo = function() {
    $.each(stepTwo, function(i, element) {
      $(element)
        .delay(1000)
        .show("slide", {direction: "right"});
    });
  };
  var hideSuccessPanel = function() {
    $('#video-submissions .success')
      .delay(700)
      .show("slide", {direction: "right"});
    $('#video-submissions .success').remove();
  };
  var showSuccessPanel = function() {
    $("#alerts .success").clone().appendTo( "#video-submissions" );
    showElementWithEffect("#video-submissions .success");
  };
  var hideErrorPanel = function() {
    hideElementWithEffect('#video-submissions .error');
    $('#video-submissions .error').remove();
  };
  var showErrorPanel = function() {
    $( "#alerts .error" ).clone().appendTo( "#video-submissions" );
    showElementWithEffect('#video-submissions .error');
  };
  var setFullWidth = function() {
    $('#video-submissions')
      .css("max-width", "100%")
      .css("width", "100%");
  };
  var hideWizard = function() {
    $.each(wizard, function(i, element) {
      hideElementWithEffect(element);
    });
  }
  var showWizard = function() {
    $.each(wizard, function(i, element) {
      showElementWithEffect(element);
    });
  }

  // Cameratag API Observers
  CameraTag.observe('grit', 'published', function(){
    hideWizard(); showSuccessPanel();
  });
  CameraTag.observe('grit', 'publishFailed', function(){
    hideWizard();
    attachErrorHandler(resetCamera);
    showErrorPanel();
  });
  CameraTag.observe('grit', 'uploadFailed', function(){
    hideStepTwo();
    attachErrorHandler(retryUpload);
    showErrorPanel();
  });

  CameraTag.observe('grit', 'cameraDenied', function(){
    alert('serverError');
  });

  // Handlers for capturing errors from cameratag's JS API
  // Do best to recover when possible, otherwise reset form
  // so UI doesn't break down.
  var resetCamera = function(e) {
    e.preventDefault();
    var thisCamera = CameraTag.cameras["grit"];
    thisCamera.reset();
    hideErrorPanel();
    showWizard();
  }
  var retryUpload = function(e) {
    e.preventDefault();
    var thisCamera = CameraTag.cameras["grit"];
    thisCamera.restartUpload();
    hideErrorPanel();
    showWizard();
  }

});