$(document).ready(function(){

  $('#carousel')
    .flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: true,
      slideshow: true,
      itemWidth: 140,
      itemMargin: 0,
      prevText: "",           
      nextText: "", 
      move: 1,
      asNavFor: '#slider'
  });
   
  $('#slider')
    .fitVids()
    .flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: true,
      slideshow: true,
      video: true,
      pauseOnHover: true,   
      sync: "#carousel"
  });

  // When a YT iframe is ready, this function gets fired
  function onYouTubeIframeAPIReady() {
    // Loop over iframes with class 'youtube'
    $('iframe.youtube').each(function(i) {
      var player = new YT.Player($(this).get()[0], {events: {
        'onReady': myYT.onPlayerReady,
        'onStateChange': myYT.onPlayerStateChange
      }
    });
    // Reference each iframe and store the object for retrieval
    $(this).data('index', i);
    myYT.players.push(player);
  });
  }
  var myYT = {
    done: false,
    initFlag: true,
    init: function(){
      var tag, firstScriptTag;
      // Insert the iframe_api script on first run
      if (myYT.initFlag) {
        tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        myYT.initFlag = false;
      }
    },
    players: [],
    onPlayerReady: function(event) {
      event.target.playVideo();
    },
    onPlayerStateChange: function(event) {
      if (event.data == YT.PlayerState.PLAYING && !myYT.done) {
        setTimeout(stopVideo, 6000);
        myYT.done = true;
      }
      function stopVideo(event) {
        myYT.stopVideo(event.target);
      }
    },
    stopVideo: function(player) {
      player.stopVideo();
    }
  }
  myYT.init();

});