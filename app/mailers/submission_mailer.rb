class SubmissionMailer < ActionMailer::Base
  default from: "mailer@thisisgrit.us"
end
