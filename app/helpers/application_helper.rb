module ApplicationHelper

def nav_link_to(link_text, link_path)
  class_name = current_page?(link_path) ? 'active' : ''

  content_tag(:li, :class => class_name) do
    link_to link_text, link_path
  end
end

def icon(name, size=1)
    #icon("camera-retro")
    #<i class="icon-camera-retro"></i> 
    
    html = "<i class='icon-#{name}' "
    html += "style='font-size:#{size}em' "
    html += "></i>"
    html.html_safe
  end

end
