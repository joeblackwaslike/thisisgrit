json.array!(@video_jobs) do |video_job|
  json.extract! video_job, :id
  json.url video_job_url(video_job, format: :json)
end
