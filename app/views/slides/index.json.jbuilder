json.array!(@slides) do |slide|
  json.extract! slide, :id, :image, :width, :height, :url, :order
  json.url slide_url(slide, format: :json)
end
