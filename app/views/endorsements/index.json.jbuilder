json.array!(@endorsements) do |endorsement|
  json.extract! endorsement, :id, :content, :credit, :order
  json.url endorsement_url(endorsement, format: :json)
end
