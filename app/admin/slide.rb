ActiveAdmin.register Slide do
  config.sort_order = "order asc"
  permit_params :image, :width, :height, :url, :orde
# 
  index do
    column "image" do |image|
      image_tag('slides/'+image.image, :height => '75')
    end
    column :order
    column :url
    column :width
    column :height
    default_actions
  end

  show do |slide|
    attributes_table do
      row :image do
        image_tag('slides/'+slide.image, :width => '400')
      end
      row :order
      row :url do
        link_to(slide.url, slide.url)
      end
      row :width
      row :height
      row :created_at
    end
    active_admin_comments
  end

  index do |slide|



  end

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
