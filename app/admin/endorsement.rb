ActiveAdmin.register Endorsement do
  permit_params :content, :credit, :order

  form do |f|
    f.inputs do
      f.input :order
      f.input :content
      f.input :credit
    end
    f.actions do
      f.action :submit, :as => :button
      f.action :cancel, :as => :link
    end
  end

  index do
    column :order
    column :content
    column :credit
    default_actions
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
