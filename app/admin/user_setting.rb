ActiveAdmin.register UserSetting do
  permit_params :key, :value, :parent_id

  index do
    column :parent do |r|
      if r.parent
        r.parent.key
      end
    end
    column :key
    column :value
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :key
      f.input :value
      f.input :parent, :as => :select, :collection => UserSetting.all.compact.map {|us| [us.key, us.id]}
    end
    f.actions do
      f.action :submit, :as => :button
      f.action :cancel, :as => :link
    end
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
