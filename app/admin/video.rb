ActiveAdmin.register Video do
  permit_params :key, :name, :email

  form do |f|
    f.inputs do
      f.input :key
      f.input :name
      f.input :email
    end
    f.actions do
      f.action :submit, :as => :button
      f.action :cancel, :as => :link
    end
  end

  show do |video|

    video.youtube_embed.html_safe
    attributes_table do
      row :id
      row :key
      row :name
      row :email
      row :youtube_url do
        video.youtube_url
      end
      row :thumb_url do
        video.thumb_url
      end
      row :youtube_embed do
        video.youtube_embed
      end
      row :views
      row :likes
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
