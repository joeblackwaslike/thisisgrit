class CameraTagHandlerController < ApplicationController
  respond_to :json

  skip_before_action :verify_authenticity_token, only: [:create]

  def create
    r = JSON.parse(request.raw_post)

    logger.debug "New Post: uuid: #{r['uuid']}, video_url: #{r['formats'][0]['video_url']}, state: #{r['formats'][0]['state']}"

    if r['uuid']
      uuid = r['uuid']
      state = r['formats'][0]['state'] || 'np'
      quality = r['formats'][0]['name'] || 'vga'
      ['name', 'email'].each do |i|
        if defined? r['metadata'][i]
          val = r['metadata'][i]  
        else
          val = ''
        end
        instance_variable_set("@#{i}", val)
      end

      v = VideoJob.find_or_initialize_by_uuid({
        uuid: uuid,
        state: state,
        quality: quality,
        name: @name,
        email: @email
      })
      if v.save!
        logger.info "NewVideoJob [SUCCESS] SAVE for UUID: #{v.uuid} ID: #{v.id}"
      else
        logger.error "NewVideoJob [ERROR] SAVE for UUID #{v.uuid} ID: #{v.id}"
      end
    end
    render json: {success: true}
  end

end
