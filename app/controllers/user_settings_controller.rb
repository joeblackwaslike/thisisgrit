class UserSettingsController < InheritedResources::Base
  before_action :authenticate_admin_user!
  private

  def set_user_setting
    @user_setting = UserSetting.find(params[:id])
  end

  def user_setting_params
    params.require(:user_setting).permit(:key, :value, :parent_id)
  end

end
