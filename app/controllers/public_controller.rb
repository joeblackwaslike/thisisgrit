class PublicController < ApplicationController
  before_filter :get_video_by_token, only: [:details]

  def index
    @slides = Video.order(views: :desc).limit(30)
    @endorsements = Endorsement.order(order: :asc)
  end

  def about
    @executive_summary = Content.find_by_slug('executive-summary')
  end

  def more
    @top_rated = Video.top_rated
    @top_viewed = Video.top_viewed
    @latest = Video.latest
  end

  def share
    @share_your_story = Content.find_by_slug('share-your-story')
    @video_script = Content.find_by_slug('video-script')
  end

  def details
    @videos = Video.latest
  end

protected

  def get_video_by_token
    if params[:token].nil?
      redirect_to action: 'more'
    end
    @video = Video.find_by_token(params[:token])
  end

end


  

  