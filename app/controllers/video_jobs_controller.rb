class VideoJobsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_video_job, only: [:show, :edit, :update, :destroy]

  # GET /video_jobs
  # GET /video_jobs.json
  def index
    @video_jobs = VideoJob.all
  end

  # GET /video_jobs/1
  # GET /video_jobs/1.json
  def show
  end

  # GET /video_jobs/new
  def new
    @video_job = VideoJob.new
  end

  # GET /video_jobs/1/edit
  def edit
  end

  # POST /video_jobs
  # POST /video_jobs.json
  def create
    @video_job = VideoJob.new(video_job_params)

    respond_to do |format|
      if @video_job.save
        format.html { redirect_to @video_job, notice: 'Video job was successfully created.' }
        format.json { render action: 'show', status: :created, location: @video_job }
      else
        format.html { render action: 'new' }
        format.json { render json: @video_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /video_jobs/1
  # PATCH/PUT /video_jobs/1.json
  def update
    respond_to do |format|
      if @video_job.update(video_job_params)
        format.html { redirect_to @video_job, notice: 'Video job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @video_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /video_jobs/1
  # DELETE /video_jobs/1.json
  def destroy
    @video_job.destroy
    respond_to do |format|
      format.html { redirect_to video_jobs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video_job
      @video_job = VideoJob.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_job_params
      params[:video_job]
    end
end
