class VideoJob < ActiveRecord::Base
  require 'youtube_it'
  require 'httparty'

  attr_accessor :video_url, :thumb_url

  URL_PREFIX='http://cameratag.com/videos/'
  VIDEO_EXT='mp4'
  THUMB_EXT='thumb'

  after_create :que_self

  def que_self
    que
  end

  def video_url
    URL_PREFIX+self.uuid+"/#{self.quality}/"+VIDEO_EXT
  end

  def thumb_url
    URL_PREFIX+self.uuid+"/#{self.quality}/"+THUMB_EXT
  end

  def que
    perform
  end
  handle_asynchronously :que

  def perform
    # download remote video file to temporary file
    tf = Tempfile.new(self.uuid)
    url = self.video_url

    tf.binmode
    tf.write(HTTParty.get(url).body)
    tf.close

    # upload file using youtube
    @yt = YoutubeService.new
    @response = @yt.upload(path: tf.path, name: self.name)
    #store video info to database
    if @response
      @v = Video.find_or_initialize_by_key(
             key: @response.unique_id,
             name: self.name,
             email: self.email
            )
      unless @v.save!
        raise 'Error Saving key to database'
      end
    else
      raise 'Error uploading to youtube'
    end
    tf.unlink
  end

  def success(job)
    logger.info "SUCCESS NewVideo [SAVE] key: #{@response.unique_id}"
    self.destroy!
  end

  def error(job, exception)
    logger.error "ERROR NewVideo [SAVE] key: #{@response.unique_id} Exception: #{exception} Job: #{job.inspect}"
  end

end
