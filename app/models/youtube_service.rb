class YoutubeService

  CONFIG = ApplicationSettings.config['youtube']
  TEMPLATE = ApplicationSettings.config['template']
  ERROR_MSGS = OpenStruct.new({:upload => 'No path provided to video file'})

  def initialize
    setup_session
  end

  def setup_session
    #@session ||= YouTubeIt::Client.new(:username => YouTubeITConfig.username , :password => YouTubeITConfig.password , :dev_key => YouTubeITConfig.dev_key)
    @session ||= YouTubeIt::Client.new(:username => CONFIG['username'], :password => CONFIG['password'], :dev_key => CONFIG['dev_key'])
  end

  def upload(params = {path: nil, name: nil})
    if params[:path].nil?
      Rails.logger.error ERROR_MSGS[:upload]
      raise ERROR_MSGS[:upload]
    end
    if params[:name].nil?
      title = TEMPLATE['title_noname']
    else
      title = TEMPLATE['title_name']+" #{params[:name]}"
    end
    @session.video_upload(File.open(params[:path]), title: title, description: TEMPLATE['description'], category: TEMPLATE['category'], tags: TEMPLATE['tags'])
  end

end
