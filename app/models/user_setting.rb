class UserSetting < ActiveRecord::Base
  belongs_to :parent, :class_name => "UserSetting", :foreign_key => "parent_id"
  has_many :user_settings, :class_name => "UserSetting", :foreign_key => "parent_id"

end
