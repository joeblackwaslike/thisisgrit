class Video < ActiveRecord::Base
  include Tokenable
  require 'youtube_it'

  attr_accessor :youtube_url, :thumb_url

  URL_PREFIX='//www.youtube.com/watch?v='
  THUMB_PREFIX='http://img.youtube.com/vi/'
  THUMB_SUFFIX='/hqdefault.jpg'

  def self.featured_videos
    @@featured_videos ||= load_featured_videos
  end

  def self.load_featured_videos
    vs = ApplicationSettings.config['featured_videos']
    videos = []
    for v in vs
      video = self.find_by_key(v['key'])
      videos << video
    end
    videos.compact!
    @@featured_videos = videos
  end

  def self.top_viewed
    self.order(views: :desc).limit(3)
  end

  def self.top_rated
    self.order(likes: :desc).limit(3)
  end

  def self.latest
    self.order(created_at: :desc).limit(3)
  end

  def youtube_url
    URL_PREFIX+self.key
  end

  def thumb_url
    THUMB_PREFIX+self.key+THUMB_SUFFIX
  end

  def youtube_object
    @s = get_session
    @s.my_video(self.key)
  end

  def json_object
    @o = youtube_object
    @o.to_json
  end

  def pretty_json
    @j = json_object
    
  end

  def youtube_embed
    video = youtube_object
    video.embed_html5
  end

  def get_session
    @yt ||= YoutubeService.new
    @session ||= @yt.setup_session
  end

  def update_counters
    #instantiate youtube
    @yt = YoutubeService.new
    @yt

    #likes
    #views

  end
 
end
