module Tokenable
  extend ActiveSupport::Concern

  included do
    before_save :generate_token
  end

  protected

  def generate_token
    unless self.token
      self.token = loop do
        random_token = SecureRandom.urlsafe_base64(8, false).first(8)
        break random_token unless self.class.exists?(token: random_token)
      end
    end
  end
end
