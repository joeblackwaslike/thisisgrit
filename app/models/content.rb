class Content < ActiveRecord::Base

  def markdown
    MarkdownService.new.render(content).html_safe
  end

end
