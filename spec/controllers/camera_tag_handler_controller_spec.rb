require 'spec_helper'

describe CameraTagHandlerController do

  describe "POST 'create'" do

    before :each do
      @entry = FactoryGirl.create(:entry)
      @incoming_params = { Json Hash }
    end

    it "updates empty summary field" do
      get 'create'
      expect(response).to be_success
    end
  end

end
