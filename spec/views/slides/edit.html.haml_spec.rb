require 'spec_helper'

describe "slides/edit" do
  before(:each) do
    @slide = assign(:slide, stub_model(Slide,
      :image => "MyString",
      :width => "MyString",
      :height => "MyString",
      :url => "MyString",
      :order => 1
    ))
  end

  it "renders the edit slide form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", slide_path(@slide), "post" do
      assert_select "input#slide_image[name=?]", "slide[image]"
      assert_select "input#slide_width[name=?]", "slide[width]"
      assert_select "input#slide_height[name=?]", "slide[height]"
      assert_select "input#slide_url[name=?]", "slide[url]"
      assert_select "input#slide_order[name=?]", "slide[order]"
    end
  end
end
