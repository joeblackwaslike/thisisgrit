require 'spec_helper'

describe "slides/show" do
  before(:each) do
    @slide = assign(:slide, stub_model(Slide,
      :image => "Image",
      :width => "Width",
      :height => "Height",
      :url => "Url",
      :order => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    expect(rendered).to match(/Image/)
    expect(rendered).to match(/Width/)
    expect(rendered).to match(/Height/)
    expect(rendered).to match(/Url/)
    expect(rendered).to match(/1/)
  end
end
