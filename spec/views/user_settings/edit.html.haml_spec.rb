require 'spec_helper'

describe "user_settings/edit" do
  before(:each) do
    @user_setting = assign(:user_setting, stub_model(UserSetting))
  end

  it "renders the edit user_setting form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", user_setting_path(@user_setting), "post" do
    end
  end
end
