require 'spec_helper'

describe "endorsements/edit" do
  before(:each) do
    @endorsement = assign(:endorsement, stub_model(Endorsement,
      :content => "MyText",
      :credit => "MyString",
      :order => 1
    ))
  end

  it "renders the edit endorsement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", endorsement_path(@endorsement), "post" do
      assert_select "textarea#endorsement_content[name=?]", "endorsement[content]"
      assert_select "input#endorsement_credit[name=?]", "endorsement[credit]"
      assert_select "input#endorsement_order[name=?]", "endorsement[order]"
    end
  end
end
