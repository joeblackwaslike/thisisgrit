require 'spec_helper'

describe "endorsements/show" do
  before(:each) do
    @endorsement = assign(:endorsement, stub_model(Endorsement,
      :content => "MyText",
      :credit => "Credit",
      :order => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Credit/)
    expect(rendered).to match(/1/)
  end
end
