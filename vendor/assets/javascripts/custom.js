$(document).ready(function(){
    // Target your .container, .wrapper, .post, etc.
    $(".video-container").fitVids();
  
//   $('.top_video_slider').bxSlider({  slideMargin: 10, useCSS: false, pager: false, auto: true, autoHover:true, }); 
//    $('.footer_sponsers').bxSlider({  slideMargin: 10, useCSS: false, pager: false  }); 
//    $('.cat_slider_1').bxSlider({  slideMargin: 10, useCSS: false, pager: true  });
  
  $('.flexslider').flexslider({
    animation: "slide"
  });

  $('#boutique').boutique({
    container_width:  'auto',
    front_img_width:  599,
    front_img_height: 'auto'
  });

  // Utility Functions
  var stepOne = ['#steps_form .step1', '#steps_count .step1'];
  var stepTwo = ['#steps_form .step2', '#steps_count .step2'];
  var wizard = ['#wizard', '#steps_count'];
  var showElementWithEffect = function(element) {
    $(element).show("slide", {direction: "left"})
  }
  var hideElementWithEffect = function(element) {
    $(element).hide("slide", {direction: "left"})
  }
  var attachErrorHandler = function(handler) {
    $('#video-submissions .error a.try-again').click(handler);
  }

  // Show, Hide Helpers
  var hideStepOne = function() {
    $.each(stepOne, function(i, element) {
      $(element).hide("slide", {direction: "left"});
    });
  };
  var showStepOne = function() {
    $.each(stepOne, function(i, element) {
      showElementWithEffect(element);
    });
  };
  var hideStepTwo = function() {
    $.each(stepTwo, function(i, element) {
      $(element).hide("slide", {direction: "left"});
    });
  };
  var showStepTwo = function() {
    $.each(stepTwo, function(i, element) {
      $(element)
        .delay(1000)
        .show("slide", {direction: "right"});
    });
  };
  var hideSuccessPanel = function() {
    $('#video-submissions .success')
      .delay(700)
      .show("slide", {direction: "right"});
    $('#video-submissions .success').remove();
  };
  var showSuccessPanel = function() {
    $("#alerts .success").clone().appendTo( "#video-submissions" );
    showElementWithEffect("#video-submissions .success");
  };
  var hideErrorPanel = function() {
    hideElementWithEffect('#video-submissions .error');
    $('#video-submissions .error').remove();
  };
  var showErrorPanel = function() {
    $( "#alerts .error" ).clone().appendTo( "#video-submissions" );
    showElementWithEffect('#video-submissions .error');
  };
  var setFullWidth = function() {
    $('#video-submissions')
      .css("max-width", "100%")
      .css("width", "100%");
  };
  var hideWizard = function() {
    $.each(wizard, function(i, element) {
      hideElementWithEffect(element);
    });
  }
  var showWizard = function() {
    $.each(wizard, function(i, element) {
      showElementWithEffect(element);
    });
  }

  // Create userDetails object, grab camera, attach userDetails to camera,
  // hide step1, show step2, set full width
  $('#video-submissions #steps_form .step1 .next').click(function(e) {
    e.preventDefault();
    var userDetails = ({
        name: $('#steps_form .step1 input[type="text"]').val(),
        email: $('#steps_form .step1 input[type="email"]').val()
    });
    var thisCamera = CameraTag.cameras["grit"];
    thisCamera.addVideoData(userDetails);
    hideStepOne();
    showStepTwo();
    setFullWidth();
    
  });
  // Cameratag API Observers
  CameraTag.observe('grit', 'published', function(){
    hideWizard(); showSuccessPanel();
  });
  CameraTag.observe('grit', 'publishFailed', function(){
    hideWizard();
    attachErrorHandler(resetCamera);
    showErrorPanel();
  });
  CameraTag.observe('grit', 'uploadFailed', function(){
    hideStepTwo();
    attachErrorHandler(retryUpload);
    showErrorPanel();
  });

  // Handlers for capturing errors from cameratag's JS API
  // Do best to recover when possible, otherwise reset form
  // so UI doesn't break down.
  var resetCamera = function(e) {
    e.preventDefault();
    var thisCamera = CameraTag.cameras["grit"];
    thisCamera.reset();
    hideErrorPanel();
    showWizard();
  }
  var retryUpload = function(e) {
    e.preventDefault();
    var thisCamera = CameraTag.cameras["grit"];
    thisCamera.restartUpload();
    hideErrorPanel();
    showWizard();
  }
  
/*  
   By default menu open upon click, to open menu on hover remove comment this code
      $('#nav .nav li,#nav .nav li').on({
    mouseenter: function() {
      $(this).children('ul').stop(true, true).slideDown(400);
      $(this).addClass('hover_menu');
    },
    mouseleave: function() {
      $(this).children('ul').slideUp(100);
      $(this).removeClass('hover_menu');
    }
  });
  
  $('#nav .nav li,#nav .nav li').on({
    mouseenter: function() {
      $(this).children('div').stop(true, true).slideDown(400);
      $(this).addClass('hover_menu');
    },
    mouseleave: function() {
      $(this).children('div').slideUp(100);
      $(this).removeClass('hover_menu');
    }
  });
  
  */
  
  $(".dropdown-toggle").click(function(e) {
    e.preventDefault();
  if($(this).next("div").is(":visible")){
  
  $(this).next("div").fadeOut("fast");
  } else {
  $(".dropdown-menu").fadeOut("fast");
  $(this).next("div").slideToggle("slow");
  }
  });
  
  $(".dropdown-toggle.list").click(function(e) {
    e.preventDefault();
  if($(this).next("ul").is(":visible")){
  
  $(this).next("ul").fadeOut("fast");
  } else {
  $(".dropdown-menu").fadeOut("fast");
  $(this).next("ul").slideToggle("slow");
  }
  });

  // DROP DOWN MENU TABS ====================================== //
  $('body').on('click', 'ul.tabs > li > a', function(e) {
  //Get Location of tab's content
  var contentLocation = $(this).attr('href');
  //Let go if not a hashed one
  if(contentLocation.charAt(0)=="#") {
  e.preventDefault();
  //Make Tab Active
  $(this).parent().siblings().children('a').removeClass('active');
  $(this).addClass('active');
  //Show Tab Content & add active class
  $(contentLocation).show().addClass('active').siblings().hide().removeClass('active');
  }
  });

    $(".btncont").click(function(){
    $(this).children('ul').addClass('hidden');
  }, function(){
    $(this).children('ul').addClass('show');
  });
  
    $('.select_css').selectbox();
   
  $("a[data-gal^='prettyPhoto']").prettyPhoto();
});

/* End of Counter */