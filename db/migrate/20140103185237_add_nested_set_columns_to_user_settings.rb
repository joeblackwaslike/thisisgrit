class AddNestedSetColumnsToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :lft, :integer
    add_column :user_settings, :rgt, :integer
  end
end
