class ChangeValueFromStringToTextForUserSettings < ActiveRecord::Migration
  def change
    change_column :user_settings, :value, :text
  end
end
