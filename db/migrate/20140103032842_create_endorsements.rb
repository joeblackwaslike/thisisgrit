class CreateEndorsements < ActiveRecord::Migration
  def change
    create_table :endorsements do |t|
      t.text :content
      t.string :credit
      t.integer :order

      t.timestamps
    end
  end
end
