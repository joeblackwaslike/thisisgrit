class AddDetailsToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :key, :string
    add_column :user_settings, :value, :string
    add_column :user_settings, :parent_id, :integer
  end
end
