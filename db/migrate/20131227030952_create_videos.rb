class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :key
      t.integer :views, :default => 0
      t.integer :likes, :default => 0
    end
  end
end