class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :image
      t.string :width
      t.string :height
      t.string :url
      t.integer :order

      t.timestamps
    end
  end
end
