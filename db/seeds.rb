# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

youtube = UserSetting.find_or_create_by_key(key: 'youtube')
youtube.user_settings.find_or_create_by_key(key: 'username', value: 'allforgrit@gmail.com')
youtube.user_settings.find_or_create_by_key(key: 'password', value: 'thisisgrit123')
youtube.user_settings.find_or_create_by_key(key: 'dev_key', value: 'AI39si450DBlucd4m8HDkhYHeoj-MJAfuLECSE7wfIa2OcrPHgofPb0yJ2cS-w1i2tM3z69XFW8bkkfCL4DsglKXLqF1NHPLrw')

template = UserSetting.find_or_create_by_key(key: 'template')
template.user_settings.find_or_create_by_key(key: 'title_name', value: "An Amazing Story You Havn't Heard From ")
template.user_settings.find_or_create_by_key(key: 'title_noname', value: "An Amazing Story You Havn't Heard")
template.user_settings.find_or_create_by_key(key: 'description', value: "http://thisisgrit.us :: This story will make you cry, smile, and feel badass all in two minutes. If it was awesome - share it with someone you know who needs some encouragement! #ThisIsGrit is bringing you rare, never-before-heard stories from amazing people to showcase the power of grit in pursuing our education and achieving our dreams. Follow us online at http://thisisgrit.us to see more stories, learn about what we are doing and help with the cause!")
template.user_settings.find_or_create_by_key(key: 'category', value: 'Education')
template.user_settings.find_or_create_by_key(key: 'tags', value: "grit, Education, Story-telling, Emotional, powerful, sad, inspiring, thisisgrit, this is grit")

featured_videos = UserSetting.find_or_create_by_key(key: 'featured_videos')
featured_videos.user_settings.find_or_create_by_value(key: 'key', value: '1SD5Y1jwQag')
featured_videos.user_settings.find_or_create_by_value(key: 'key', value: '4P0Vt6yBg1s')
featured_videos.user_settings.find_or_create_by_value(key: 'key', value: 'CFuKvaQJXTA')

Content.find_or_create_by_slug(slug: "video-script", content:"### Remember to keep your video to 1.5 - 3 minutes.\r\nMy name is *[name]*. In *[year]* I *[experience hardship that is education related]*. I *[extended description of hardship]*. \r\n        \r\nInstead of *[doing something passive and despairing]*, I decided to work harder at *[overcoming the hardship]* by *[activity]*. \r\n        \r\nI am now *[current status]*. This is Grit!")
Content.find_or_create_by_slug(slug: "share-your-story", content: "Congratulations, you've decided to submit a video! It's a scary decision, and you have #GRIT. \r\nThe purpose of these videos is not about self-promotion, though we know that you are probably ridiculously accomplished and done great things. The purpose is to share a time in your life when you've faced tremendous adversity, especially in the pursuit of your education, and struggled through it by persistence. Through your story, we want to reach the hearts and lives of children and students who are struggling now, and say: it is okay to struggle. It is necessary to struggle. \r\n\r\nSuccess depends not on your age, your gender, you race, your socio-economic background. Success is determined by one thing: your #GRIT. \r\n\r\nFill out your information below, and tell your own #GRITTY story. The more authentic you can be, the more powerful your message is. Remember to end strong with: This Is Grit!")
Content.find_or_create_by_slug(slug: "executive-summary", content: "The Obama Administration in early in 2013 challenged a national gathering of innovators, entrepreneurs and studentsto marshall their creativity and resourcefulness to help bolster the issues of student access and success for education. At the [Stanford DataJam](http://www.ed.gov/blog/2013/12/innovators-come-together-to-brainstorm-solutions-and-take-action/)Stanford DataJam (idea hackathon) hosted by the Dept. of Ed., we won first place with our project #ThisIsGRIT. \r\n\r\nThe core idea behind #ThisIsGRIT is simple: **a good education takes more than intelligence, its takes #GRIT**. Backed by studies of [famous psychological studies](http://alumni.stanford.edu/get/page/magazine/article/?article_id=32124) from Prof. Carol Dweck and popularized on a [TED talk by Angela Duckworth](http://www.ted.com/talks/angela_lee_duckworth_the_key_to_success_grit.html), **#GRIT** is the ability to persevere, to push through hurdles of all types, and is the  #1 of not only academic success, but in careers and family life as well. \r\n\r\nEach of us who have navigated the path through collegiate experiences, and on to professional achievement have a story, or two, or three of how **#GRIT** got us through hardships and to our goals.  We are trying to find people with tremendous **#GRIT** stories, so that students everywhere can be encouraged around this very important element in the formula of success.\r\n\r\nIn preparation for the **Jan 15 showcase at the Whitehouse Datapalooza**, we are building a website inspired by [Code.org](http://code.org) + [ItGetsBetter.org](http://itgetsbetter.org) , for the #gritty. We would love to use this chance to share the amazing work that Carol Dweck and Angela Duckworth have done with as many people as possible, and instill **#GRIT** in our next generation.")
#Content.find_or_create_by_slug(slug: "", content: "")

Endorsement.find_or_create_by_credit(credit: "Eduardo Briceño, CEO and CoFounder with Prof. Carol Dweck of MindSet Works", content: "Research shows that when students understand that they can develop their own intelligence and abilities they become interested in learning, they adopt a positive view of effort, and they persevere in the face of setbacks, all of which leads to higher levels of success. We support the #ThisIsGRIT experiment to use story-telling to influence key beliefs and develop grit, and we look forward to collaborating with them in that endeavor.")

Slide.find_or_create_by_image(image: "duckworth.png", order: "0", url: "/about", width: "677", height: "339")
Slide.find_or_create_by_image(image: "carols.jpg", order: "1", url: "", width: "403", height: "275")
Slide.find_or_create_by_image(image: "gritovergift.jpg", order: "2", url: "", width: "600", height: "427")
Slide.find_or_create_by_image(image: "grit.jpg", order: "3", url: "", width: "600", height: "400")
