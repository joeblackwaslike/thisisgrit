namespace :videos do

  desc "Update Video likes and views"
  task :update => :environment do
    # GEt youtube api session
    @yt = YoutubeService.new
    @yt = @yt.setup_session

    @yt_videos = @yt.my_videos.videos

    @db_videos = Video.find(:all)

    for dbv in @db_videos
      @this_v = @yt_videos.detect {|ytv| ytv.unique_id == dbv.key}
      if @this_v
        if (dbv.views != @this_v.view_count) || (dbv.likes != @this_v.favorite_count)
          dbv.views = @this_v.view_count
          dbv.likes = @this_v.favorite_count
          dbv.save!
        end
      end
    end
  end

  desc "Add videos from youtube channel that aren't in db"
  task :add => :environment do 

    # GEt youtube api session
    @yt = YoutubeService.new
    @yt = @yt.setup_session

    @yt_videos = @yt.my_videos.videos

    for ytv in @yt_videos
      unless Video.find_by_key(ytv.unique_id)
        Video.create(key: ytv.unique_id, views: ytv.view_count, likes: ytv.favorite_count)
      end
    end
  end

  desc "Delete videos in database that arent in channel anymore(probably been deleted manually"
  task :clean => :environment do

    # GEt youtube api session
    @yt = YoutubeService.new
    @yt = @yt.setup_session

    @db_videos = Video.where("created_at < ?", 30.minutes.ago)
    @yt_videos = @yt.my_videos.videos

    for dbv in @db_videos
      @yt_videos.each do |ytv| 
        Video.find_or_initialize_by_key(v.key).destroy!
        ytv.key == v.key 
      end  
    end
  end

  desc "Tokenizes old videos in database"
  task :tokenize => :environment do

    @db_videos = Video.find(:all)

    for dbv in @db_videos
      dbv.save!  
    end
  end

end